using System.Text;

public static class Identifier
{
    public static string Clean(string identifier)
    {
        var stringBuilder = new StringBuilder();
        var isPrecededByDash = false;

        foreach (var ch in identifier)
        {
            stringBuilder.Append(ch switch
            {
                _ when isPrecededByDash => char.ToUpperInvariant(ch),
                _ when IsLowercaseGreekChar(ch) => string.Empty,
                _ when char.IsWhiteSpace(ch) => '_',
                _ when char.IsControl(ch) => "CTRL",
                _ when char.IsLetter(ch) => ch,
                _ => string.Empty
            });

            isPrecededByDash = ch.Equals('-');
        }

        return stringBuilder.ToString();
    }

    private static bool IsLowercaseGreekChar(char ch)
    {
        return ch is >= 'α' and <= 'ω';
    }
}