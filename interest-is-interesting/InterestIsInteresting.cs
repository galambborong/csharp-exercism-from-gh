static class SavingsAccount
{
    public static float InterestRate(decimal balance) => balance switch
    {
        < 0m => 3.213f,
        < 1000m => 0.5f,
        _ when balance.IsBetween1000And5000() => 1.621f,
        _ => 2.475f
    };

    public static decimal Interest(decimal balance)
    {
        var balanceWithInterest = balance * (decimal) InterestRate(balance);

        return balanceWithInterest / 100;
    }

    public static decimal AnnualBalanceUpdate(decimal balance) => balance + Interest(balance);

    public static int YearsBeforeDesiredBalance(decimal balance, decimal targetBalance)
    {
        var counter = 0;

        while (balance < targetBalance)
        {
            balance = AnnualBalanceUpdate(balance);
            counter++;
        }

        return counter;
    }

    private static bool IsBetween1000And5000(this decimal balance)
    {
        const decimal upperValue = 5000m;
        const decimal lowerValue = 1000m;
        return balance is >= lowerValue and < upperValue;
    }
}